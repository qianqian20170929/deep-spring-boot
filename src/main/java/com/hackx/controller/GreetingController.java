package com.hackx.controller;

import com.hackx.entity.GreetingEntity;
import com.hackx.entity.HelloMessageEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

/**
 * Created by 曹磊(Hackx) on 13/10/2017.
 * Email: caolei@mobike.com
 */
@Controller
public class GreetingController {

    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public GreetingEntity greeting(HelloMessageEntity message) throws Exception {
        Thread.sleep(1000); // simulated delay
        return new GreetingEntity("Hello, " + message.getName() + "!");
    }
}
