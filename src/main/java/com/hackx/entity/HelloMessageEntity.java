package com.hackx.entity;

import java.io.Serializable;

/**
 * Created by 曹磊(Hackx) on 13/10/2017.
 * Email: caolei@mobike.com
 */
public class HelloMessageEntity implements Serializable {

    private static final long serialVersionUID = 793563805927839455L;

    private String name;

    public HelloMessageEntity() {
    }

    public HelloMessageEntity(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
