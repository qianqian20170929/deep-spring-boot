package com.hackx.entity;

import java.io.Serializable;

/**
 * Created by 曹磊(Hackx) on 13/10/2017.
 * Email: caolei@mobike.com
 */
public class GreetingEntity implements Serializable {

    private static final long serialVersionUID = -6925076160412493214L;

    private String content;

    public GreetingEntity() {
    }

    public GreetingEntity(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

}
